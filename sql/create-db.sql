-- DROP database IF EXISTS "periodical-literature";

 CREATE database "periodical-literature";

\c "periodical-litertature";



CREATE TABLE literature (
                        id INT PRIMARY KEY generated always as identity ,
                        name varchar(45) NOT NULL,
                        sub_price  NUMERIC default 0.0,
                        description VARCHAR DEFAULT Null

);

CREATE TABLE roles (
                       id INT PRIMARY KEY generated always as identity,
                       name VARCHAR(45) NOT NULL
);


CREATE TABLE users (
                       id INT PRIMARY KEY generated always as identity,
                       login VARCHAR(45) UNIQUE,
                       password VARCHAR(120) ,
                       email VARCHAR(45),
                       funds NUMERIC default 0.0,
                       first_name VARCHAR(45),
                       last_name VARCHAR(45),
                       role_id INT REFERENCES roles(id)  on delete cascade DEFAULT null,
                       isBanned boolean DEFAULT false
);

CREATE TABLE genres(
                       id INT PRIMARY KEY generated always as identity,
                       genre VARCHAR(45) UNIQUE
);

CREATE TABLE literatures_genres(
                        literature_id INT REFERENCES literature(id) on delete cascade,
                        genre_id INT REFERENCES genres(id) on delete cascade,
                        UNIQUE (literature_id,genre_id)
);


CREATE TABLE users_literatures (
                        user_id INT REFERENCES users(id) on delete cascade,
                        literature_id INT REFERENCES literature(id) on delete cascade,
                        expired_time timestamp DEFAULT now()+ INTERVAL '1 month',
                        UNIQUE (user_id, literature_id)
);


Create or replace function fn()
    returns trigger
as $$ begin
    if new.role_id is null then
        new.role_id = (select roles.id from roles where name = 'User');
    end if;
    return new;
end;
$$ language plpgsql;

CREATE TRIGGER
    fn
    BEFORE INSERT ON
    users
    FOR EACH ROW EXECUTE PROCEDURE
    fn();

INSERT INTO roles values (DEFAULT, 'User');
INSERT into roles VALUES (DEFAULT, 'Admin');



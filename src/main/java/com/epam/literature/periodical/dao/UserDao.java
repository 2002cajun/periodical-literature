package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.entity.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * Data access object for User table
 */
public interface UserDao {
   boolean insertUser(User user) throws DBException;
   User getUserByLoginPassword (String login, String password) throws DBException;
   List<User> getAllUsers()throws DBException;
   boolean updateUserIsBanned(int userId,boolean isBanned) throws DBException;
   boolean updateUserFunds (int userId, BigDecimal funds)throws DBException;
}

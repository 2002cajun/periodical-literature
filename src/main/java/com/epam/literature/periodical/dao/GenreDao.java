package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.entity.Genre;

import java.util.List;

/**
 * Data access object for Genre table
 *
 */
public interface GenreDao {

    boolean insertGenre(Genre genre) throws DBException;
    boolean deleteGenre(int genreId);
    boolean updateGenre(Genre genre) throws DBException;
    Genre getGenreById(int genreId) throws DBException;
    List<Genre> getAllGenres() throws DBException;

}

package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Data access object for UsersLiteratures table
 */
public interface UserPeriodicalLiteratureDao {
    LocalDateTime getExpiredTimeByUserIdLiteratureId (int userId, int literatureId) throws DBException;
    boolean deleteUserSubscriptionByUserIdLiteratureId(int userId, int literatureId) throws DBException;
    boolean insertUserPeriodicalLiteratureByUserIdLiteratureId(int userId, int litratureId) throws DBException;
    List<PeriodicalLiterature> getAllUserLiteraturesById(int userId) throws DBException;
}

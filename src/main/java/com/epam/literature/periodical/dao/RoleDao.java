package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.entity.Role;

/**
 * Data access object for Role table
 */
public interface RoleDao {
    Role getRoleByName(String name) throws DBException;
}

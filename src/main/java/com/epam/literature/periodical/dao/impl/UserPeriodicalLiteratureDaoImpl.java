package com.epam.literature.periodical.dao.impl;

import com.epam.literature.periodical.dao.ConnectionPool;
import com.epam.literature.periodical.dao.Query;
import com.epam.literature.periodical.dao.UserPeriodicalLiteratureDao;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserPeriodicalLiteratureDaoImpl implements UserPeriodicalLiteratureDao {
    private static UserPeriodicalLiteratureDaoImpl instance;

    private ConnectionPool connectionPool = ConnectionPool.getInstance();


    public static synchronized UserPeriodicalLiteratureDaoImpl getInstance() {
        if (instance == null) {
            instance = new UserPeriodicalLiteratureDaoImpl();
        }
        return instance;
    }

    private UserPeriodicalLiteratureDaoImpl() {

    }

    @Override
    public LocalDateTime getExpiredTimeByUserIdLiteratureId(int userId, int literatureId) throws DBException {
        LocalDateTime dateTime;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_SUBSCRIPTION_END_TIME);
            stmt.setInt(1, literatureId);
            stmt.setInt(2,userId);

            rs = stmt.executeQuery();

            if(!rs.next()){
                return null;
            }
            dateTime = rs.getObject("expired_time", LocalDateTime.class);
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting user subscription expire time error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }
        return dateTime;
    }

    @Override
    public boolean deleteUserSubscriptionByUserIdLiteratureId(int userId, int literatureId) throws DBException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.DELETE_USER_SUBSCRIPTION_BY_USERID_LITERATURE_ID);

            stmt.setInt(1,userId);
            stmt.setInt(2,literatureId);

            result = stmt.execute();

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting user error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        return result;
    }

    @Override
    public boolean insertUserPeriodicalLiteratureByUserIdLiteratureId(int userId, int litratureId) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.INSERT_USER_LITERATURE_BY_USER_ID_LITERATURE_ID);

            stmt.setInt(1,userId);
            stmt.setInt(2,litratureId);
            count = stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("Insert user literature error",e);
        } finally {
            connectionPool.close(stmt);
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        if(count > 0){
            return true;
        }
        return false;
    }

    @Override
    public List<PeriodicalLiterature> getAllUserLiteraturesById(int userId) throws DBException {
        List<PeriodicalLiterature> literatures = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_USER_LITERATURES_BY_ID);

            stmt.setInt(1,userId);
            rs = stmt.executeQuery();


            while (rs.next()) {
                PeriodicalLiterature literature = new PeriodicalLiterature();
                literature.setName(rs.getString("name"));
                literature.setId(rs.getInt("id"));
                literature.setSubscriptionPrice(rs.getBigDecimal("sub_price"));
                literature.setDescription(rs.getString("description"));
                literatures.add(literature);
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting literatures error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }

        return literatures;
    }

}

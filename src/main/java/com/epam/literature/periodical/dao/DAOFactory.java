package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.dao.impl.*;


/**
 *Factory class of DAO implementations
 */
public class DAOFactory {

    public static GenreDao getGenreDao(){
        return GenreDaoImpl.getInstance();
    }

    public static UserDao getUserDao(){
        return UserDaoImpl.getInstance();
    }

    public static RoleDao getRoleDao(){
        return RoleDaoImpl.getInstance();
    }

    public static PeriodicalLiteratureDao getPeriodicalLiteratureDao(){
        return PeriodicalLiteratureDaoImpl.getInstance();
    }

    public static UserPeriodicalLiteratureDao getUserPeriodicalLiteratureDao(){
        return UserPeriodicalLiteratureDaoImpl.getInstance();
    }

    public static LiteraturesGenresDao getLiteraturesGenresDaoImpl(){
        return LiteraturesGenresDaoImpl.getInstance();
    }

    public static UserRoleDao getUserRoleDao(){
        return UserRoleDaoImpl.getInstance();
    }

}

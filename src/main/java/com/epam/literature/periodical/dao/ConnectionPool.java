package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.dao.impl.UserDaoImpl;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * ConnectionPool.
 * Configure context.xml for your database
 *
 *
 */

public class ConnectionPool {

    private static ConnectionPool instance;
    private DataSource dataSource;

    public static synchronized ConnectionPool getInstance() {
        if (instance == null) {
            instance = new ConnectionPool();
        }
        return instance;
    }

    ConnectionPool(){

    }
    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in your
     * WEB_APP_ROOT/META-INF/context.xml file.
     *
     *
     * @return A DB connection.
     */

    public Connection getConnection(){
        if(dataSource==null) {
            try {
                Context initContext = new InitialContext();
                Context envContext = (Context) initContext.lookup("java:comp/env");
                dataSource = (javax.sql.DataSource) envContext.lookup("jdbc/periodical-literature");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
    /**
     * Close the given resource.
     *
     * @param autoCloseable
     *            Resource to be closed
     */

    public void close(AutoCloseable autoCloseable) {
        try {
            if(autoCloseable!=null)
                autoCloseable.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Rollbacks the given connection.
     *
     * @param con
     *            Connection to be rollbacked.
     */

    public void rollback(Connection con) {
        try {
            con.rollback();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}

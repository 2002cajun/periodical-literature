package com.epam.literature.periodical.dao.impl;

import com.epam.literature.periodical.dao.ConnectionPool;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.dao.RoleDao;
import com.epam.literature.periodical.entity.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Data access object for Role table
 * @see com.epam.literature.periodical.dao.RoleDao
 */
public class RoleDaoImpl implements RoleDao {
    private static RoleDaoImpl instance;

    ConnectionPool connectionPool = ConnectionPool.getInstance();

    RoleDaoImpl(){

    }

    public static synchronized RoleDaoImpl getInstance() {
        if (instance == null) {
            instance = new RoleDaoImpl();
        }
        return instance;
    }
    @Override
    public Role getRoleByName(String name)throws DBException {
        Role role = new Role();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement("SELECT * FROM roles WHERE name = ?");

            stmt.setString(1, name);

            rs = stmt.executeQuery();
            if(rs.next() == false){
                return null;
            }
            role.setName(rs.getString("name"));
            role.setId(rs.getInt("id"));

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }
        return role;
    }
}

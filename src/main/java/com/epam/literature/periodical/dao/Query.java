package com.epam.literature.periodical.dao;

/**
 * Class with queries for database
 */
public final class Query {
    public static final String INSERT_USER = "INSERT INTO users (id,login,password,email,first_name,last_name,role_id,isBanned) VALUES (DEFAULT,?,?,?,?,?,DEFAULT,DEFAULT)";
    public static final String GET_USER_BY_LOGIN_PASS = "SELECT users.id,login,name,last_name,first_name,funds,email,role_id, isBanned " +
                                                           "FROM users JOIN roles on  (login,password,users.role_id) = (?,?,roles.id);";
    public static final String GET_GENRE_BY_ID = "SELECT * FROM genres WHERE id = ?";
    public static final String GET_ALL_LITERATURES_BY_PATTERN = "SELECT * FROM literature WHERE lower(name) LIKE ?";
    public static final String GET_ALL_LITERATURES = "SELECT * FROM literature";
    public static final String GET_ALL_GENRES_BY_LITERATURE_ID = "SELECT genre_id , genre from literatures_genres join genres on  (literature_id,genre_id) = (?,genres.id)";
    public static final String GET_ALL_USERS = "SELECT * FROM users JOIN roles r on r.id = users.role_id";
    public static final String GET_SUBSCRIPTION_END_TIME = "SELECT expired_time FROM users_literatures WHERE (literature_id,user_id) = (?,?)";
    public static final String GET_LITERATURE_BY_ID = "SELECT * FROM literature WHERE id = ?";
    public static final String DELETE_USER_SUBSCRIPTION_BY_USERID_LITERATURE_ID = "DELETE FROM users_literatures WHERE (user_id,literature_id) = (?,?)";
    public static final String UPDATE_USER_IS_BANNED = "UPDATE users SET isBanned = ? WHERE id = ?";
    public static final String UPDATE_USER_FUNDS = "UPDATE users SET funds = ? WHERE id = ?";
    public static final String INSERT_USER_LITERATURE_BY_USER_ID_LITERATURE_ID = "INSERT INTO users_literatures (user_id, literature_id,expired_time) values (?,?,DEFAULT)";
    public static final String DELETE_PERIODICAL_LITERATURE_BY_ID = "DELETE FROM literature WHERE id = ?";
    public static final String GET_USER_LITERATURES_BY_ID = "SELECT literature.id,literature.description,literature.sub_price,literature.name from literature JOIN users_literatures on literature.id = users_literatures.literature_id where user_id  = ?";
    public static final String DELETE_LITERATURE_GENRE_BY_ID = "DELETE FROM literatures_genres WHERE (literature_id,genre_id) = (?,?)";
    public static final String UPDATE_LITERATURE = "UPDATE literature SET (name,sub_price,description) = (?,?,?) WHERE id = ?";
    public static final String GET_ALL_GENRES = "SELECT * FROM genres";
    public static final String INSERT_LITERATURE_GENRE = "INSERT INTO literatures_genres (literature_id, genre_id) VALUES (?,?)";
    public static final String INSERT_GENRE = "INSERT INTO genres (id,genre) values (DEFAULT,?)";
    public static final String INSERT_LITERATURE = "INSERT INTO literature (id,name, sub_price, description) VALUES (DEFAULT,?,?,?)";

}

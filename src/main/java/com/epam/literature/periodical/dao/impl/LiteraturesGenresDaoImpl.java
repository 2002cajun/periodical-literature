package com.epam.literature.periodical.dao.impl;

import com.epam.literature.periodical.dao.ConnectionPool;
import com.epam.literature.periodical.dao.LiteraturesGenresDao;
import com.epam.literature.periodical.dao.Query;
import com.epam.literature.periodical.entity.Genre;
import com.epam.literature.periodical.exceptions.DBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for LiteraturesGenres table
 * @see com.epam.literature.periodical.dao.LiteraturesGenresDao
 */

public class LiteraturesGenresDaoImpl implements LiteraturesGenresDao {
    private static LiteraturesGenresDaoImpl instance;

    ConnectionPool connectionPool = ConnectionPool.getInstance();

    LiteraturesGenresDaoImpl() {

    }

    public static synchronized LiteraturesGenresDaoImpl getInstance() {
        if (instance == null) {
            instance = new LiteraturesGenresDaoImpl();
        }
        return instance;
    }

    @Override
    public List<Genre> getAllGenresByLiteratureId(int id) throws DBException {
        List<Genre> genres = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_ALL_GENRES_BY_LITERATURE_ID);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Genre genre = new Genre();
                genre.setGenre(rs.getString("genre"));
                genre.setId(rs.getInt("genre_id"));
                genres.add(genre);
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting genre error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }
        return genres;
    }

    @Override
    public boolean deleteLiteratureGenre(int literatureId, int genreId) throws DBException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.DELETE_LITERATURE_GENRE_BY_ID);

            stmt.setInt(1,literatureId);
            stmt.setInt(2,genreId);

            result = stmt.execute();

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("delete literature genre error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        return result;
    }

    @Override
    public boolean insertLiteratureGenre(int literatureId, int genreId) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.INSERT_LITERATURE_GENRE);


            stmt.setInt(1,literatureId);
            stmt.setInt(2,genreId);

            stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("Insert literature genre error",e);
        } finally {
            connectionPool.close(stmt);
            connectionPool.close(con);
        }

        return true;

    }
}


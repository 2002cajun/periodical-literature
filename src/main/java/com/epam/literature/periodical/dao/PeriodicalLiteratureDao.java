package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;
import java.util.List;

/**
 * Data access object for PeriodicalLiterature table
 */

public interface PeriodicalLiteratureDao {
    List<PeriodicalLiterature> getAllLiteratures() throws DBException;
    List<PeriodicalLiterature> getAllLiteraturesByPattern(String pattern) throws DBException;
    PeriodicalLiterature getLiteratureById(int literatureId) throws DBException;
    boolean deletePeriodicalLiteratureById(int literatureId) throws DBException;
    boolean updatePeriodicalLiterature(PeriodicalLiterature literature) throws DBException;
    boolean insertLiterature(PeriodicalLiterature literature) throws DBException;

}

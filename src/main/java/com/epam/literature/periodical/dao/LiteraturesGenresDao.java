package com.epam.literature.periodical.dao;

import com.epam.literature.periodical.entity.Genre;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;

import java.util.List;

/**
 * Data access object for LiteraturesGenres table
 */
public interface LiteraturesGenresDao {
    List<Genre> getAllGenresByLiteratureId(int id) throws DBException;
    boolean deleteLiteratureGenre(int literatureId,int genreId) throws DBException;
    boolean insertLiteratureGenre(int literatureId,int genreId) throws DBException;
}

package com.epam.literature.periodical.dao.impl;

import com.epam.literature.periodical.dao.ConnectionPool;
import com.epam.literature.periodical.dao.Query;
import com.epam.literature.periodical.entity.Role;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.dao.UserDao;
import com.epam.literature.periodical.entity.User;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Data access object for User table
 * @see com.epam.literature.periodical.dao.UserDao
 */
public class UserDaoImpl implements UserDao {

    private static UserDaoImpl instance;

    private ConnectionPool connectionPool = ConnectionPool.getInstance();


    public static synchronized UserDaoImpl getInstance() {
        if (instance == null) {
            instance = new UserDaoImpl();
        }
        return instance;
    }

    private UserDaoImpl() {

    }

    @Override
    public boolean insertUser(User user) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.INSERT_USER,
                    Statement.RETURN_GENERATED_KEYS);


            stmt.setString(1, user.getLogin());
            stmt.setString(2, user.getPassword());
            stmt.setString(3,user.getEmail());
            stmt.setString(4,user.getFirstName());
            stmt.setString(5,user.getLastName());

            count = stmt.executeUpdate();
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if(generatedKeys.next()){
                int i = generatedKeys.getInt(1);
                user.setId(i);
            }

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("Insert error",e);
        } finally {
            connectionPool.close(stmt);
            connectionPool.close(con);
        }
        if(count > 0){
            return true;
        }
        return false;
    }

    @Override
    public User getUserByLoginPassword(String login, String password) throws DBException {
        User user = new User();
        Role role = new Role();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_USER_BY_LOGIN_PASS);
            stmt.setString(1, login);
            stmt.setString(2,password);
            rs = stmt.executeQuery();

            if(rs.next() == false){
                return null;
            }
            user.setId(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
            user.setEmail(rs.getString("email"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setFunds(rs.getBigDecimal("funds"));
            user.setBanned(rs.getBoolean("isBanned"));
            role.setId(rs.getInt("role_id"));
            role.setName(rs.getString("name"));
            user.setRole(role);
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting user error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }
        return user;
    }

    @Override
    public List<User> getAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_ALL_USERS);
            rs = stmt.executeQuery();


            while (rs.next()) {
                User user = new User();
                Role role = new Role();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                user.setEmail(rs.getString("email"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setFunds(rs.getBigDecimal("funds"));
                user.setBanned(rs.getBoolean("isBanned"));
                role.setId(rs.getInt("role_id"));
                role.setName(rs.getString("name"));
                user.setRole(role);
                users.add(user);
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting users error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }

        return users;
    }

    @Override
    public boolean updateUserIsBanned(int userId, boolean isBanned) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.UPDATE_USER_IS_BANNED);


            stmt.setBoolean(1, isBanned);
            stmt.setInt(2, userId);

            count = stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("update user error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        if(count > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean updateUserFunds(int userId, BigDecimal funds) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.UPDATE_USER_FUNDS);


            stmt.setBigDecimal(1, funds);
            stmt.setInt(2, userId);

            count = stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("update user error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        if(count > 0){
            return true;
        }
        return false;
    }


}

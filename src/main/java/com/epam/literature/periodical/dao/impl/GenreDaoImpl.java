package com.epam.literature.periodical.dao.impl;

import com.epam.literature.periodical.dao.ConnectionPool;
import com.epam.literature.periodical.dao.Query;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.dao.GenreDao;
import com.epam.literature.periodical.entity.Genre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Data access object for Genre table
 * @see com.epam.literature.periodical.dao.GenreDao
 *//**
 * Data access object for Genre table
 * @see com.epam.literature.periodical.dao.GenreDao
 */

public class GenreDaoImpl implements GenreDao {

    private static GenreDaoImpl instance;

    ConnectionPool connectionPool = ConnectionPool.getInstance();

    GenreDaoImpl(){

    }

    public static synchronized GenreDaoImpl getInstance() {
        if (instance == null) {
            instance = new GenreDaoImpl();
        }
        return instance;
    }

    @Override
    public boolean insertGenre(Genre genre) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.INSERT_GENRE);


            stmt.setString(1, genre.getGenre());


            count = stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("Insert error",e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        if(count > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteGenre(int genreId) {
        return false;
    }

    @Override
    public boolean updateGenre(Genre genre) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement("UPDATE genres SET genre = ?  WHERE id = (?)");


            stmt.setString(1, genre.getGenre());
            stmt.setInt(2, genre.getId());

            count = stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("update error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        if(count > 0){
            return true;
        }
        return false;
    }

    @Override
    public Genre getGenreById(int genreId) throws DBException {
        Genre genre = new Genre();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.GET_GENRE_BY_ID);

            stmt.setInt(1, genreId);

            rs = stmt.executeQuery();
            if(rs.next() == false){
                return null;
            }
            genre.setGenre(rs.getString("genre"));
            genre.setId(rs.getInt("id"));

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }
        return genre;
    }

    @Override
    public List<Genre> getAllGenres() throws DBException {
        List<Genre> genres = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_ALL_GENRES);
            rs = stmt.executeQuery();


            while (rs.next()) {
                Genre genre = new Genre();
                genre.setGenre(rs.getString("genre"));
                genre.setId(rs.getInt("id"));
                genres.add(genre);
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting genres error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }

        return genres;
    }

}

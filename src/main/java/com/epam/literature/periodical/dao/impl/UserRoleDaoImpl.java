package com.epam.literature.periodical.dao.impl;

import com.epam.literature.periodical.dao.ConnectionPool;
import com.epam.literature.periodical.dao.UserRoleDao;

/**
 * Data access object for UserRole table
 *
 */
public class UserRoleDaoImpl implements UserRoleDao {
    private static UserRoleDaoImpl instance;

    ConnectionPool connectionPool = ConnectionPool.getInstance();

    UserRoleDaoImpl(){

    }

    public static synchronized UserRoleDaoImpl getInstance() {
        if (instance == null) {
            instance = new UserRoleDaoImpl();
        }
        return instance;
    }


}

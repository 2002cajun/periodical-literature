package com.epam.literature.periodical.dao.impl;

import com.epam.literature.periodical.dao.ConnectionPool;
import com.epam.literature.periodical.dao.PeriodicalLiteratureDao;
import com.epam.literature.periodical.dao.Query;
import com.epam.literature.periodical.entity.Genre;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.Role;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for PeriodicalLiterature table
 * @see com.epam.literature.periodical.dao.PeriodicalLiteratureDao
 */

public class PeriodicalLiteratureDaoImpl implements PeriodicalLiteratureDao {
    private static PeriodicalLiteratureDaoImpl instance;

    ConnectionPool connectionPool = ConnectionPool.getInstance();

    PeriodicalLiteratureDaoImpl(){

    }

    public static synchronized PeriodicalLiteratureDaoImpl getInstance() {
        if (instance == null) {
            instance = new PeriodicalLiteratureDaoImpl();
        }
        return instance;
    }


    @Override
    public List<PeriodicalLiterature> getAllLiteratures() throws DBException {
        List<PeriodicalLiterature> literatures = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_ALL_LITERATURES);
            rs = stmt.executeQuery();


            while (rs.next()) {
                PeriodicalLiterature literature = new PeriodicalLiterature();
                literature.setName(rs.getString("name"));
                literature.setId(rs.getInt("id"));
                literature.setSubscriptionPrice(rs.getBigDecimal("sub_price"));
                literature.setDescription(rs.getString("description"));
                literatures.add(literature);
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting literatures error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }

        return literatures;
    }

    @Override
    public List<PeriodicalLiterature> getAllLiteraturesByPattern(String pattern) throws DBException {
        List<PeriodicalLiterature> literatures = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_ALL_LITERATURES_BY_PATTERN);
            stmt.setString(1,pattern);
            rs = stmt.executeQuery();


            while (rs.next()) {
                PeriodicalLiterature literature = new PeriodicalLiterature();
                literature.setName(rs.getString("name"));
                literature.setId(rs.getInt("id"));
                literature.setSubscriptionPrice(rs.getBigDecimal("sub_price"));
                literature.setDescription(rs.getString("description"));
                literatures.add(literature);
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting literatures error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }

        return literatures;
    }

    @Override
    public PeriodicalLiterature getLiteratureById(int literatureId) throws DBException {
        PeriodicalLiterature literature = new PeriodicalLiterature();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.GET_LITERATURE_BY_ID);
            stmt.setInt(1,literatureId);
            rs = stmt.executeQuery();

            if(rs.next() == false){
                return null;
            }
            literature.setName(rs.getString("name"));
            literature.setId(rs.getInt("id"));
            literature.setSubscriptionPrice(rs.getBigDecimal("sub_price"));
            literature.setDescription(rs.getString("description"));
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("getting literature error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
            connectionPool.close(rs);
        }
        return literature;
    }

    @Override
    public boolean deletePeriodicalLiteratureById(int literatureId) throws DBException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement(Query.DELETE_PERIODICAL_LITERATURE_BY_ID);

            stmt.setInt(1,literatureId);

            result = stmt.execute();

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("deleting literature error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        return result;
    }

    @Override
    public boolean updatePeriodicalLiterature(PeriodicalLiterature literature) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.UPDATE_LITERATURE);


            stmt.setString(1, literature.getName());
            stmt.setBigDecimal(2, literature.getSubscriptionPrice());
            stmt.setString(3,literature.getDescription());
            stmt.setInt(4,literature.getId());

            count = stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("update literature error", e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        if(count > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean insertLiterature(PeriodicalLiterature literature) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        int count = 0;
        try {
            con = connectionPool.getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            stmt = con.prepareStatement(Query.INSERT_LITERATURE);


            stmt.setString(1, literature.getName());
            stmt.setBigDecimal(2,literature.getSubscriptionPrice());
            stmt.setString(3,literature.getDescription());


            count = stmt.executeUpdate();

            con.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            connectionPool.rollback(con);
            throw new DBException("Insert literature error",e);
        } finally {
            connectionPool.close(con);
            connectionPool.close(stmt);
        }
        if(count > 0){
            return true;
        }
        return false;
    }

}

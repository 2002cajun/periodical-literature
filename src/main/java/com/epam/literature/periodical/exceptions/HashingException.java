package com.epam.literature.periodical.exceptions;

/**
 * HashingException  class for handling exceptions occur with db
 */
public class HashingException extends Throwable{
    public HashingException(String message, Throwable cause) {
        super(message,cause);
    }
}

package com.epam.literature.periodical.exceptions;

/**
 * DBException class for handling exceptions occur with db
 */
public class DBException extends Throwable{

    public DBException(String message, Throwable cause) {
        super(message,cause);
    }
}

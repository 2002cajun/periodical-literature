package com.epam.literature.periodical.filters;

import com.epam.literature.periodical.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Login filter
 *
 *
 */

public class LoginFilter implements Filter {


    @Override
    public void init(FilterConfig config) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        if(user == null)
            req.getRequestDispatcher("/login.jsp").forward(request,response);
        else
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}

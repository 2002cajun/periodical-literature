package com.epam.literature.periodical.controller;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.exceptions.HashingException;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * User login servlet
 * Login user if exists
 *
 * @author Cajun
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private final UserService userService = new UserService();
    private static final Logger LOG = Logger.getLogger(LoginServlet.class);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user;
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        try {
            user = userService.getUserByLoginPassword(login,password);
        } catch (HashingException | DBException e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage(),e);
        }
        if(user==null){
            req.setAttribute("message","Incorrect password or login");
            getServletContext().getRequestDispatcher("/login.jsp").forward(req,resp);

        }else if(user.isBanned()){
            req.setAttribute("message","You are banned");
            getServletContext().getRequestDispatcher("/login.jsp").forward(req,resp);
        } else {
            LOG.info(user.getLogin() +  " logged in");
            HttpSession session = req.getSession(true);
            session.setAttribute("user", user);
            resp.sendRedirect(req.getContextPath());
        }
    }

}

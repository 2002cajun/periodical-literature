package com.epam.literature.periodical.controller;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/literature_profile")
public class LiteratureProfileServlet extends HttpServlet {
    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final UserService userService = new UserService();
    private static final Logger LOG = Logger.getLogger(LiteratureProfileServlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int literatureId = Integer.parseInt(req.getParameter("literatureId"));
        PeriodicalLiterature literature;
        boolean isSubbed = false;
        try {
            literature = literatureService.getLiteratureById(literatureId);
        } catch (DBException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
            throw new ServletException(e.getMessage(),e);
        }
        User user = (User) req.getSession().getAttribute("user");
        if(user!=null) {
            try {
                isSubbed = userService.subscriptionCheck(user.getId(),literatureId);
            } catch (DBException e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
                throw new ServletException(e.getMessage(),e);
            }
        }
        req.setAttribute("literature",literature);
        req.setAttribute("isSubbed",isSubbed);
        getServletContext().getRequestDispatcher("/literature_profile.jsp").forward(req,resp);

    }
}

package com.epam.literature.periodical.controller;

import com.epam.literature.periodical.entity.Role;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.HashingException;
import com.epam.literature.periodical.service.RoleService;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    private UserService userService = new UserService();
    private RoleService roleService = new RoleService();
    private static final Logger LOG = Logger.getLogger(RegistrationServlet.class);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        user.setLogin(req.getParameter("login"));
        user.setEmail(req.getParameter("email"));
        user.setFirstName(req.getParameter("firstName"));
        user.setLastName(req.getParameter("lastName"));
        user.setPassword(req.getParameter("password"));

        try {
            userService.registerUser(user);
            LOG.info("user " + user.getLogin() + " has been created");
        } catch (DBException | HashingException e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage(), e);
        }

        resp.sendRedirect(req.getContextPath() + "/login.jsp");
    }
}

package com.epam.literature.periodical.controller;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user_profile")
public class UserProfileServlet extends HttpServlet {
    UserService userService = new UserService();
    private final Logger LOG = Logger.getLogger(UserProfileServlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       User user = (User) req.getSession().getAttribute("user");
        List<PeriodicalLiterature> literatures;
        try {
            literatures = userService.getAllUserLiteraturesById(user.getId());
        } catch (DBException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
            throw new ServletException(e.getMessage(),e);
        }
        req.setAttribute("literatures",literatures);
        getServletContext().getRequestDispatcher("/user_profile.jsp").forward(req,resp);

    }
}

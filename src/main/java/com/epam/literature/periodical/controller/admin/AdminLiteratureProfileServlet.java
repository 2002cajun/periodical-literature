package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.controller.LiteratureProfileServlet;
import com.epam.literature.periodical.entity.Genre;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.GenreService;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Admin literature profile servlet
 * Servlet gets data for literature profile
 *
 * @author Cajun
 */
@WebServlet("/admin/literature_profile")
public class AdminLiteratureProfileServlet extends HttpServlet {
    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final GenreService genreService = new GenreService();
    private static final Logger LOG = Logger.getLogger(AdminLiteratureProfileServlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int literatureId = Integer.parseInt(req.getParameter("literatureId"));
        List<Genre> genres;
        PeriodicalLiterature literature;
        try {
            genres = genreService.getAllGenres();
            literature = literatureService.getLiteratureById(literatureId);
        } catch (DBException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
            throw new ServletException(e.getMessage(),e);
        }
        List<Genre> excludeGenres = new ArrayList<>();
        if(genres!=null&&literature!=null){
            for (Genre genre:genres) {
                if(!literature.getGenres().contains(genre))
                    excludeGenres.add(genre);
            }

        }

        req.setAttribute("genres",excludeGenres);
        req.setAttribute("literature",literature);
        getServletContext().getRequestDispatcher("/admin/edit_literature.jsp").forward(req,resp);

    }
}

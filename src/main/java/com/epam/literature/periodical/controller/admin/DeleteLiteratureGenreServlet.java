package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Delete literature genre servlet.
 * Delete genre for exact literature
 *
 * @author Cajun
 */
@WebServlet("/admin/delete_genre")
public class DeleteLiteratureGenreServlet extends HttpServlet {

    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final Logger LOG = Logger.getLogger(DeleteLiteratureGenreServlet.class);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int genreId = Integer.parseInt(req.getParameter("genreId"));
        int literatureId = Integer.parseInt(req.getParameter("literatureId"));
        try {
            literatureService.deleteLiteratureGenre(literatureId,genreId);
        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }
        resp.sendRedirect(req.getHeader("referer"));

    }
}

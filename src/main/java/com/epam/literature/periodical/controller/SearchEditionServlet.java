package com.epam.literature.periodical.controller;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/searchEdition")
public class SearchEditionServlet extends HttpServlet {
    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final Logger LOG = Logger.getLogger(SearchEditionServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String price = req.getParameter("price");
        String[] genres = req.getParameterValues("genre");
        String search = req.getParameter("search");

        List<PeriodicalLiterature> literatures;
        try {
                literatures= literatureService.getAllLiteratures();
                
        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }

        if(search!=null&&!literatures.isEmpty())
            literatures = literatureService.filterAllLiteraturesByPattern(search,literatures);
        if(genres!=null&&!literatures.isEmpty())
            for (String genre:genres) {
                literatures = literatureService.filterLiteraturesByGenre(genre,literatures);
            }
        req.setAttribute("literatures",literatures);
        getServletContext().getRequestDispatcher("/search.jsp").forward(req,resp);

    }
}

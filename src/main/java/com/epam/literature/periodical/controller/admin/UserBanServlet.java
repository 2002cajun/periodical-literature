package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User ban servlet.
 * Ban user
 *
 * @author Cajun
 */
@WebServlet("/admin/user_ban")
public class UserBanServlet extends HttpServlet {
    private final UserService userService = new UserService();
    private static final Logger LOG = Logger.getLogger(UserBanServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Boolean ban = Boolean.valueOf(req.getParameter("ban"));
        int userId = Integer.parseInt(req.getParameter("id"));
        try {
            userService.banUser(userId,ban);
        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }
        resp.sendRedirect(req.getHeader("referer"));
    }
}

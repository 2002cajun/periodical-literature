package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * Admin edition search servlet.
 * Servlet searches for periodical literature
 *
 * @author Cajun
 */
@WebServlet("/admin/edition_search")
public class AdminEditionSearchServlet extends HttpServlet {
    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final Logger LOG = Logger.getLogger(AdminEditionSearchServlet.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String search = req.getParameter("search");

        List<PeriodicalLiterature> literatures;
        try {
            literatures= literatureService.getAllLiteratures();

        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }

        if(search!=null&&!literatures.isEmpty())
            literatures = literatureService.filterAllLiteraturesByPattern(search,literatures);

        req.setAttribute("literatures",literatures);
        getServletContext().getRequestDispatcher("/admin/edition_search.jsp").forward(req,resp);
    }
}

package com.epam.literature.periodical.controller;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Edition Servlet.
 * Servlet gets all literature edition.
 *
 * @author Cajun
 */
@WebServlet("/edition")
public class EditionServlet extends HttpServlet {
    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final UserService userService = new UserService();
    private static final Logger LOG = Logger.getLogger(EditionServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<PeriodicalLiterature> literatures;
        try {
            literatures = literatureService.getAllLiteratures();
        } catch (DBException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
            throw new ServletException(e.getMessage(),e);
        }
        req.setAttribute("literatures", literatures);
        getServletContext().getRequestDispatcher("/edition.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

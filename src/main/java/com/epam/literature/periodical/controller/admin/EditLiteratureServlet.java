package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Edit literature servlet.
 * Edit periodical literature from database
 *
 * @author Cajun
 */
@WebServlet("/admin/edit_literature")
public class EditLiteratureServlet extends HttpServlet {
    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private static final Logger LOG = Logger.getLogger(EditLiteratureServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PeriodicalLiterature literature = new PeriodicalLiterature();
        literature.setName(req.getParameter("literature"));
        literature.setDescription(req.getParameter("description"));
        literature.setSubscriptionPrice(BigDecimal.valueOf(Double.parseDouble(req.getParameter("price"))));
        literature.setId(Integer.parseInt(req.getParameter("literatureId")));

        try {
            literatureService.updateLiterature(literature);
        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }
        User user = (User) req.getSession().getAttribute("user");
        LOG.info(literature.getName() + "has benn edited by id by" + user.getRole() + " " + user.getLogin());
        resp.sendRedirect(req.getHeader("referer"));
    }
}

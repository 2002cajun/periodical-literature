package com.epam.literature.periodical.controller;

import com.epam.literature.periodical.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * User logout servlet
 * Logout user if exists
 *
 * @author Cajun
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(LogoutServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if(session.getAttribute("user")!=null) {
            User user = (User)session.getAttribute("user");
            session.removeAttribute("user");
            LOG.info(user.getLogin() + " logged out");
        }
        resp.sendRedirect(req.getContextPath());
    }

}

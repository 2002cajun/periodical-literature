package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.entity.Genre;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.GenreService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Add Genre servlet
 * Servlet adds genre to database
 *
 * @author Cajun
 */
@WebServlet("/admin/add_genre")
public class AddGenreServlet extends HttpServlet {
    private final GenreService genreService = new GenreService();
    private final Logger LOG = Logger.getLogger(AddGenreServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       String genreName = req.getParameter("genre");
       Genre genre = new Genre();
       genre.setGenre(genreName);
        try {
            genreService.addGenre(genre);
        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }
        resp.sendRedirect(req.getHeader("referer"));

    }
}

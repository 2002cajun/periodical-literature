package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Delete literature servlet.
 * Delete exact periodical literature from database
 *
 * @author Cajun
 */
@WebServlet("/admin/delete_literature")
public class DeleteLiteratureServlet extends HttpServlet {

    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private static final Logger LOG = Logger.getLogger(DeleteLiteratureServlet.class);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int literatureId = Integer.parseInt(req.getParameter("id"));
        try {
            literatureService.deletePeriodicalLiterature(literatureId);
        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }
        User user = (User) req.getSession().getAttribute("user");
        LOG.info(literatureId + "was deleted by id by"+user.getRole() + " " + user.getLogin());
        resp.sendRedirect(req.getHeader("referer"));
    }
}

package com.epam.literature.periodical.controller;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/subscribe")
public class SubscriptionServlet extends HttpServlet {

    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final UserService userService = new UserService();
    private static final Logger LOG = Logger.getLogger(SubscriptionServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int literatureId = Integer.parseInt(req.getParameter("id"));
        PeriodicalLiterature literature = null;
        User user = (User) req.getSession().getAttribute("user");
        boolean isSubbed= false;
        try {
            literature = literatureService.getLiteratureById(literatureId);
            isSubbed = userService.subscriptionCheck(user.getId(),literatureId);
        } catch (DBException e) {
            e.printStackTrace();
        }

        if(isSubbed){
            //something to do if subbed
        }else {
            if (literature != null) {
                if(user.getFunds().compareTo(literature.getSubscriptionPrice())>=0) {
                    try {
                        userService.subscribe(user,literature);
                        LOG.info(user.getLogin() + "has subbed for" + literature.getName());
                    } catch (DBException e) {
                        e.printStackTrace();
                        LOG.error(e.getMessage());
                        throw new ServletException(e.getMessage(),e);
                    }
                    resp.sendRedirect(req.getContextPath()+"/literature_profile?literatureId=" + literatureId);
                }else{
                    req.setAttribute("message","you don't have enough funds to subscribe");
                    req.setAttribute("literature",literature);
                    req.setAttribute("isSubbed",isSubbed);
                    getServletContext().getRequestDispatcher("/literature_profile.jsp").forward(req,resp);
                }
            }
        }
    }
}

package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.PeriodicalLiteratureService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Add Literature servlet
 * Servlet adds literature to database
 *
 * @author Cajun
 */
@WebServlet("/admin/add_literature")
public class AddLiteratureServlet extends HttpServlet {
    private final PeriodicalLiteratureService literatureService = new PeriodicalLiteratureService();
    private final Logger LOG = Logger.getLogger(AddLiteratureServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PeriodicalLiterature literature = new PeriodicalLiterature();
        literature.setName(req.getParameter("literature"));
        literature.setDescription(req.getParameter("description"));
        literature.setSubscriptionPrice(BigDecimal.valueOf(Double.parseDouble(req.getParameter("price"))));

        try {
            literatureService.addLiterature(literature);
        } catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }
        User user = (User) req.getSession().getAttribute("user");
        LOG.info(literature.getName() + "has benn added by id by" + user.getRole() + " " + user.getLogin());
        resp.sendRedirect(req.getHeader("referer"));
    }
}

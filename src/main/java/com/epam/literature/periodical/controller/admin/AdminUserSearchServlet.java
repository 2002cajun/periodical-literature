package com.epam.literature.periodical.controller.admin;

import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * User search servlet.
 * Servlet searches for user
 *
 * @author Cajun
 */
@WebServlet("/admin/search_user")
public class AdminUserSearchServlet extends HttpServlet {
    private final UserService userService = new UserService();
    private final Logger LOG = Logger.getLogger(AdminUserSearchServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String search = req.getParameter("search");
        List<User> users;
        try {
            users = userService.getALlUsers();
            System.out.println(users);
            users.forEach(System.out::println);
           }
         catch (DBException e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
            throw new ServletException(e.getMessage(),e);
        }
        if(search!=null&&!users.isEmpty()){
            users = userService.getAllUsersByPattern(search,users);
        }
        users.forEach(user -> System.out.println(user.getLogin()));
        req.setAttribute("users",users);
        getServletContext().getRequestDispatcher("/admin/user_search.jsp").forward(req,resp);

    }
}

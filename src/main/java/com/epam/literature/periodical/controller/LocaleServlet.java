package com.epam.literature.periodical.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Locale servlet
 * Changes locale
 *
 * @author Cajun
 */
@WebServlet("/locale")
public class LocaleServlet extends EditionServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       req.getSession().setAttribute( "lang" ,req.getParameter("lang"));
       resp.sendRedirect(req.getHeader("referer"));
    }
}

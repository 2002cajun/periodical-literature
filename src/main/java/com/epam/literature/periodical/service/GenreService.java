package com.epam.literature.periodical.service;

import com.epam.literature.periodical.dao.DAOFactory;
import com.epam.literature.periodical.dao.GenreDao;
import com.epam.literature.periodical.entity.Genre;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;

import java.util.List;

/**
 * GenreService.
 * Class to work with genre entity.
 * @see com.epam.literature.periodical.entity.Genre
 *
 */
public class GenreService {
    GenreDao genreDao = DAOFactory.getGenreDao();

    public boolean addGenre(Genre genre) throws DBException {
        return genreDao.insertGenre(genre);
    }

    public List<Genre> getAllGenres() throws DBException {
        return genreDao.getAllGenres();
    }
}

package com.epam.literature.periodical.service;

import com.epam.literature.periodical.dao.*;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;
import com.epam.literature.periodical.entity.User;
import com.epam.literature.periodical.exceptions.HashingException;
import org.apache.log4j.Logger;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * UserService.
 * Class to work with user entity.
 * @see User;
 */

public class UserService {

    private static final Logger LOG = Logger.getLogger(UserService.class);
    private UserPeriodicalLiteratureDao userLiteratureDao = DAOFactory.getUserPeriodicalLiteratureDao();
    private UserDao userDao = DAOFactory.getUserDao();
    private LiteraturesGenresDao literaturesGenresDao = DAOFactory.getLiteraturesGenresDaoImpl();


    public List<User> getALlUsers() throws DBException {
       return userDao.getAllUsers();
    }

    public List<User> getAllUsersByPattern(String pattern, List<User> users)  {
        return users.stream().filter(user -> user.getLogin().toLowerCase(Locale.ROOT).
                contains(pattern.toLowerCase(Locale.ROOT))).collect(Collectors.toList());

    }

    /**
     * Get user by password and login
     * to check if user exists
     *
     *
     * @return
     *         User if exists , null otherwise
     */
    public User getUserByLoginPassword(String login, String password) throws DBException, HashingException {
        return userDao.getUserByLoginPassword(login,passwordHashing(password));
    }

    /**
     * Register user
     *
     * @param user
     *            User.
     * @return
     *          boolean if user has been registered
     */
    public boolean registerUser(User user) throws DBException, HashingException {
        String hashedPassword = passwordHashing(user.getPassword());
        user.setPassword(hashedPassword);
        return userDao.insertUser(user);
    }

    /**
     * Hashing password that user sent
     *
     * @param password
     *            User password.
     * @return hashed password
     */
    private String passwordHashing(String password) throws HashingException {
        byte[] salt = new byte[16];
        for (byte i = 0; i < salt.length; i++) {
            salt[i]=  i++;
        }
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKeyFactory factory;
        byte[] hash;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = factory.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            LOG.error("hashing password error");
            throw new HashingException("Hashing password exception",e);
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            stringBuilder.append(hash[i]);
        }
        return stringBuilder.toString();
    }

    /**
     * Check user subscription for literature

     *
     * @return
     *         if user is subscriber for literature
     */
    public boolean subscriptionCheck(int userId, int editionId) throws DBException {
        LocalDateTime nowDateTime = LocalDateTime.now();
        LocalDateTime dateTime = userLiteratureDao.getExpiredTimeByUserIdLiteratureId(userId,editionId);
        if(dateTime==null)
            return false;
        else if (dateTime.isBefore(nowDateTime)) {
            userLiteratureDao.deleteUserSubscriptionByUserIdLiteratureId(userId,editionId);
            return false;
        }
        else
            return true;
    }

    public boolean banUser(int userId, boolean toBan) throws DBException {
        return userDao.updateUserIsBanned(userId,toBan);
    }

    public boolean subscribe(User user,PeriodicalLiterature literature) throws DBException {
        BigDecimal userFunds = user.getFunds();
        BigDecimal literaturePrice = literature.getSubscriptionPrice();
        userFunds = userFunds.subtract(literaturePrice);
        userFunds = userFunds.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        System.out.println("vse ok 2");
        userDao.updateUserFunds(user.getId(),userFunds);
        user.setFunds(userFunds);
        System.out.println("vse ok 1");
        return userLiteratureDao.insertUserPeriodicalLiteratureByUserIdLiteratureId(user.getId(),literature.getId());
    }
    public List<PeriodicalLiterature> getAllUserLiteraturesById(int userId) throws DBException {
        List<PeriodicalLiterature> literatures = userLiteratureDao.getAllUserLiteraturesById(userId);
        for (PeriodicalLiterature literature:literatures) {
            literature.setGenres(literaturesGenresDao.getAllGenresByLiteratureId(literature.getId()));
        }
        return literatures;
    }
}


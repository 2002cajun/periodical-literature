package com.epam.literature.periodical.service;

import com.epam.literature.periodical.dao.DAOFactory;
import com.epam.literature.periodical.dao.LiteraturesGenresDao;
import com.epam.literature.periodical.dao.PeriodicalLiteratureDao;
import com.epam.literature.periodical.entity.PeriodicalLiterature;
import com.epam.literature.periodical.exceptions.DBException;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * PeriodicalLiteratureService.
 * Class to work with periodical literature entity.
 * @see PeriodicalLiterature
 *
 */
public class PeriodicalLiteratureService {
    PeriodicalLiteratureDao literatureDao = DAOFactory.getPeriodicalLiteratureDao();
    LiteraturesGenresDao literaturesGenresDao = DAOFactory.getLiteraturesGenresDaoImpl();


    public List<PeriodicalLiterature> getAllLiteratures() throws DBException {
        List<PeriodicalLiterature> literatures = literatureDao.getAllLiteratures();
        if (!literatures.isEmpty()) {
            for (PeriodicalLiterature literature : literatures) {
                literature.setGenres(literaturesGenresDao.getAllGenresByLiteratureId(literature.getId()));
            }
        }
        return literatures;
    }
    public List<PeriodicalLiterature> filterAllLiteraturesByPattern(String pattern,List<PeriodicalLiterature> literatures) {
          return literatures.stream().filter(literature -> literature.getName().toLowerCase(Locale.ROOT)
                   .contains(pattern.toLowerCase(Locale.ROOT))).collect(Collectors.toList());

        }

    public List<PeriodicalLiterature> filterLiteraturesByGenre(String genre,List<PeriodicalLiterature> literatures){
       return literatures.stream().filter(literature -> literature.getGenres().stream()
                        .anyMatch(genre1 -> genre1.getGenre().toLowerCase(Locale.ROOT).contains(genre.toLowerCase(Locale.ROOT))))
                .collect(Collectors.toList());
    }

    public List<PeriodicalLiterature> getAllLiteratureByGenre(String genre){
        genre = genre.toLowerCase(Locale.ROOT);
        List<PeriodicalLiterature> literatures;
        return null;
    }

    public PeriodicalLiterature getLiteratureById(int literatureId) throws DBException {
        PeriodicalLiterature literature = literatureDao.getLiteratureById(literatureId);
        if(literature!=null)
            literature.setGenres(literaturesGenresDao.getAllGenresByLiteratureId(literature.getId()));
        return literature;
    }

    public boolean deletePeriodicalLiterature(int literatureId) throws DBException {
        return literatureDao.deletePeriodicalLiteratureById(literatureId);
    }

    public boolean deleteLiteratureGenre(int literatureId, int genreId) throws DBException {
        return literaturesGenresDao.deleteLiteratureGenre(literatureId,genreId);
    }

    public boolean updateLiterature(PeriodicalLiterature literature)throws DBException{
        return literatureDao.updatePeriodicalLiterature(literature);
    }

    public boolean addLiteratureGenre(int literatureId, int genreId) throws DBException {
        return literaturesGenresDao.insertLiteratureGenre(literatureId,genreId);
    }

    public boolean addLiterature(PeriodicalLiterature literature)throws DBException{
       return literatureDao.insertLiterature(literature);
    }

}

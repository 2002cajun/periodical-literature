package com.epam.literature.periodical.entity;


import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

/**
 * User entity.
 *
 * @author Cajun
 *
 */

public class User implements Serializable {


    private static final long serialVersionUID = -5731813990181596684L;


    private int id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    private Set<PeriodicalLiterature> subscriptions;
    private BigDecimal funds;
    private LocalDate expiredTime;
    private boolean isBanned;


    public boolean isBanned() {
        return isBanned;
    }

    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<PeriodicalLiterature> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Set<PeriodicalLiterature> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public BigDecimal getFunds() {
        return funds;
    }

    public void setFunds(BigDecimal funds) {
        this.funds = funds;
    }

    public LocalDate getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(LocalDate expiredTime) {
        this.expiredTime = expiredTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}

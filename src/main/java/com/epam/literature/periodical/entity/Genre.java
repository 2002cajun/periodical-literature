package com.epam.literature.periodical.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * Genre entity.
 *
 * @author Cajun
 *
 */
public class Genre implements Serializable {


    private static final long serialVersionUID = -260218062594804755L;

    private  int id;
    private String genre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;
        Genre genre1 = (Genre) o;
        return getId() == genre1.getId() && Objects.equals(getGenre(), genre1.getGenre());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGenre());
    }
}

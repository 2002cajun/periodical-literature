package com.epam.literature.periodical.entity;

import java.io.Serializable;

/**
 * Role entity.
 *
 * @author Cajun
 *
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 6284456932108831332L;

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

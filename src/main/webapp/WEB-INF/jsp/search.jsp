
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>SearchResult</title>
</head>
<body>

    <c:if test="${not empty requestScope.literatures}">
    <table>
        <c:forEach var="literature" items="${requestScope.literatures}">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/literature_profile?literatureId=${literature.id}">${literature.name} </a>
                </td>
                <c:forEach var="genre" items="${literature.genres}">
                    <td>
                        <a href="${pageContext.request.contextPath}/searchEdition?genre=${genre.genre}">${genre.genre}</a>
                    </td>
                </c:forEach>

            </tr>
        </c:forEach>
    </table>

    </c:if>

<c:if test="${empty requestScope.literatures}">
        <h2><fmt:message key="nothing_found"/> </h2>
    </c:if>
</html>


<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <c:if test="${empty sessionScope.user}">
    <div align="center">
        <h1><fmt:message key="header.login"/> </h1>
        <form action="<%= request.getContextPath() %>/login" method="post">
            <table>
                <tr>
                    <td>Login</td>
                    <td><input type="text" id="login" name="login" required size="30" minlength="4"/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" name="password" required size="30" minlength="4"/></td>
            </table>
            <input type="submit" value="Submit" />
        </form>
        <c:if test="${not empty requestScope.message}">
            <h3>${requestScope.message}</h3>
        </c:if>
    </div>
    </c:if>
    <c:if test="${not empty sessionScope.user}">
        <jsp:forward page="/"/>
    </c:if>
</body>
</html>

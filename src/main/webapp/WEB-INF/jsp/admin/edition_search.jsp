<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <title>Admin</title>
</head>
<body>
<c:if test="${not empty requestScope.literatures}">

    <c:forEach var="literature" items="${requestScope.literatures}">
        <table>
            <td>
                <a href="${pageContext.request.contextPath}/literature_profile?literatureId=${literature.id}">${literature.name} </a>
            </td>
            <c:forEach var="genre" items="${literature.genres}">
                <td>
                    <a href="${pageContext.request.contextPath}/searchEdition?genre=${genre.genre}">${genre.genre}</a>
                </td>
            </c:forEach>
        </table>
        <form action="${pageContext.request.contextPath}/admin/literature_profile" METHOD="GET">
            <input type="hidden" name="literatureId" value="${literature.id}" />
            <button type="submit">Edit</button>
        </form>
        <form action="${pageContext.request.contextPath}admin/delete_literature" METHOD="POST">
            <input type="hidden" name="id" value="${literature.id}" />
            <button type="submit">Delete</button>
        </form>

        <hr>
    </c:forEach>
</c:if>


<c:if test="${empty requestScope.literatures}">
    <h2><fmt:message key="nothing_found"/> </h2>
</c:if>
</body>
</html>


<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title><fmt:message key="profile.admin"/> </title>
</head>
<body>
<div class="container">
        <div class="login-container">
            <form action="${pageContext.request.contextPath}/admin/search_user" METHOD="get">
                <input placeholder="<fmt:message key="profile.searchUser"/>" name="search" type="text" />
                <button type="submit"><fmt:message key="header.search"/> </button>
            </form>
        </div>
        <div class="login-container">
            <form action="${pageContext.request.contextPath}/admin/edition_search" METHOD="get">
                <input placeholder="<fmt:message key="header.search"/>" name="search" type="text"/>
                <button type="submit"><fmt:message key="header.search"/> </button>
            </form>
        </div>

    <table>
        <tr>
            <h3>${sessionScope.user.role.name} ${sessionScope.user.login}</h3>
        </tr>
        <tr>
            <h3>${sessionScope.user.firstName} ${sessionScope.user.lastName}</h3>
        </tr>
        <a href=""></a>
        </tr>


    </table>
    <div class="login-container">
        <form action="${pageContext.request.contextPath}/admin/add_genre" METHOD="post">
            <input  name="genre" type="text" />
            <button type="submit">Add Genre</button>
        </form>
    </div>
    <div class="login-container">
        <form action="${pageContext.request.contextPath}/admin/add_literature" METHOD="post">
            <table>
                <tr>
                    <td>LiteratureName</td>
                    <td><input type="text"  name="literature" required size="30" minlength="4"/></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><input type="text" name="description" required  minlength="4"/></td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td><input type="text" name="price" required /></td>
                </tr>
            </table>
            <input type="submit" value="Add Literature" />
        </form>
    </div>
</div>
</body>
</html>

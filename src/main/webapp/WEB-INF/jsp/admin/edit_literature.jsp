<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Edit Literature</title>
</head>
<body>
<c:if test="${not empty requestScope.literature}">
    <h3>${requestScope.literature.name}</h3>
    <h3>Price: ${requestScope.literature.subscriptionPrice}$</h3>
    <c:forEach var="genre" items="${requestScope.literature.genres}">
        <table>
            <tr>
                <form action="${pageContext.request.contextPath}/admin/delete_genre" METHOD="POST">
                    <h4>${genre.genre}</h4>
                    <input type="hidden" name="genreId" value="${genre.id}" />
                    <input type="hidden" name="literatureId" value="${requestScope.literature.id}" />
                    <button type="submit">Delete</button>
                </form>
            <tr>
        </table>
    </c:forEach>

    <h4><fmt:message key="about"/>: ${requestScope.literature.description}</h4>
    <form action="${pageContext.request.contextPath}/admin/edit_literature" METHOD="POST">
        <table>
            <tr>
                <td>LiteratureName</td>
                <td><input type="text"  name="literature" required size="30" minlength="4"/></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><input type="text" name="description" required minlength="4"/></td>
            </tr>
            <tr>
                <td>Price</td>
                <td><input type="text" required minlength="1" name="price"  /></td>
            </tr>
        </table>
        <input type="hidden" name="literatureId" value="${requestScope.literature.id}" />
        <input type="submit" value="Edit" />
    </form>
    <c:if test="${not empty requestScope.genres}">
        <table>
        <c:forEach var="genre" items="${requestScope.genres}">
            <form action="${pageContext.request.contextPath}/admin/addGenreLiterature" METHOD="POST">
                <tr>
                    <td>${genre.genre}</td>
                    <td><input type="hidden" name="genreId" value="${genre.id}" /> </td>
                    <td><input type="hidden" name="literatureId" value="${requestScope.literature.id}" /> </td>
                    <td><input type="submit" value="Add genre to Literature" /> </td>
                </tr>
            </form>
        </c:forEach>
        </table>
    </c:if>
    <form action="${pageContext.request.contextPath}/admin/add_genre" METHOD="post">
        <input  name="genre" type="text" />
        <button type="submit">Add Genre</button>
    </form>
</c:if>


<c:if test="${not empty requestScope.message}">
    <h3>${requestScope.message}</h3>
</c:if>
</body>
</html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:if test="${not empty requestScope.users}">

    <c:forEach var="user" items="${requestScope.users}">
        <table>
            <td>
                <h4>${user.role.name}</h4>
            </td>
            <td>
                <h4>${user.login}</h4>
            </td>
            <td>
                <h4>${user.email}</h4>
            </td>
            <td>
                <c:if test="${user.banned}">

                    <form action="${pageContext.request.contextPath}/admin/user_ban" METHOD="POST">
                        <input type="hidden" name="ban" value="false" />
                        <input type="hidden" name="id" value="${user.id}" />
                        <button type="submit">Unban</button>
                    </form>
                </c:if>
                <c:if test="${not user.banned}">
                    <form action="${pageContext.request.contextPath}/user_ban" METHOD="POST">
                        <input type="hidden" name="ban" value="true" />
                        <input type="hidden" name="id" value="${user.id}" />
                        <button type="submit">Ban</button>
                    </form>
                </c:if>
            </td>
        </table>
        <hr>
    </c:forEach>
</c:if>


<c:if test="${empty requestScope.users}">
    <h2><fmt:message key="nothing_found"/> </h2>
</c:if>
</body>
</html>

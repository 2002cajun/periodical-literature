
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<div align="center">
    <h1>Registration Form</h1>
    <form action="<%= request.getContextPath() %>/registration" method="post">
        <table>
            <tr>
                <td>Login</td>
                <td><input type="text" id="login" name="login" required size="30" minlength="4"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"  required size="30" minlength="4"/></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" required size="30" minlength="4"/></td>
            </tr>
            <tr>
                <td>First Name</td>
                <td><input type="text" name="firstName" required size="30" minlength="2"/></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><input type="text" name="lastName" required size="30" minlength="2"/></td>
            </tr>
        </table>
        <input type="submit" value="Submit" />
    </form>

<%--    <script>--%>
<%--        function validation(){--%>
<%--            var user = document.getElementById("login");--%>
<%--            if(user.value.length <= 20 && user.value.length >= 3){--%>
<%--            }--%>
<%--            else{--%>
<%--                alert("Username has to be between 3-20 characters.")--%>
<%--            }--%>
<%--            //duplication data list--%>
<%--            // var user = document.getElementById("login");--%>
<%--            // if(user.value == list.value){--%>
<%--            // }--%>
<%--            // else{--%>
<%--            //     alert("Username already exists.")--%>
<%--            // }--%>
<%--        }--%>
<%--    </script>--%>
</div>
</body>
</html>

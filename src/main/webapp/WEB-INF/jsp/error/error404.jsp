
<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<html>
<head>
    <title>404</title>
</head>
<body>
    <div class="container">
        <h1>404</h1></br>
        <h3>Page not found</h3>
    </div>
</body>
</html>

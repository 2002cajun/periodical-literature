
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <title>Profile</title>
</head>
<body>
<div class="container">
    <table class="sortable">
        <tr>
            <h3>${sessionScope.user.role.name} ${sessionScope.user.login}</h3>
        </tr>
        <tr>
            <h3>${sessionScope.user.firstName} ${sessionScope.user.lastName}</h3>

            <c:if test="${not empty requestScope.literatures}">
            <h2><fmt:message key="your_subs"/></h2>
                <table>
                    <c:forEach var="literature" items="${requestScope.literatures}">
                        <tr>
                            <td>
                                <a href="${pageContext.request.contextPath}/literature_profile?literatureId=${literature.id}">${literature.name} </a>
                            </td>
                            <c:forEach var="genre" items="${literature.genres}">
                                <td>
                                    <a href="${pageContext.request.contextPath}/searchEdition?genre=${genre.genre}">${genre.genre}</a>
                                </td>
                            </c:forEach>

                        </tr>
                    </c:forEach>
                </table>
            </c:if>


    </table>

</div>
</body>
</html>

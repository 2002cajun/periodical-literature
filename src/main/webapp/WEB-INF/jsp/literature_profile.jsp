<%@ include file="/WEB-INF/jspf/head.jspf" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>${requestScope.literature.name}</title>
</head>
<body>

        <c:if test="${not empty requestScope.literature}">
            <h3>${requestScope.literature.name}</h3>
                <c:forEach var="genre" items="${requestScope.literature.genres}">
                    <table>
                        <tr>
                            <a href="${pageContext.request.contextPath}/searchEdition?genre=${genre.genre}">${genre.genre}</a>
                        <tr>
                    </table>
                </c:forEach>

            <c:if test="${requestScope.isSubbed}">
                <h3><fmt:message key="you_are_subscriber"/></h3>
<%--                <h3>Price: ${requestScope.literature.subscriptionPrice}$</h3>--%>
<%--                <form action="${pageContext.request.contextPath}/subscribe" method="post">--%>
<%--                    <input type="hidden" name="id" value="${requestScope.literature.id}" />--%>
<%--                    <button type="submit"><fmt:message key="unsubscribe"/></button>--%>
<%--                </form>--%>
            </c:if>
            <c:if test="${not requestScope.isSubbed}">
                <h3>Price: ${requestScope.literature.subscriptionPrice}$</h3>
                <form action="${pageContext.request.contextPath}/subscribe" method="post">
                    <input type="hidden" name="id" value="${requestScope.literature.id}" />
                    <button type="submit"><fmt:message key="subscribe"/></button>
                </form>
            </c:if>

            <c:if test="${not empty requestScope.literature.description}">
                <h4><fmt:message key="about"/>: ${requestScope.literature.description}</h4>
            </c:if>

        </c:if>



    <c:if test="${not empty requestScope.message}">
        <h3>${requestScope.message}</h3>
    </c:if>

</body>
</html>

package com.epam.literature.periodical.controller;

import org.junit.Test;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class LogoutServletTest {

    final String path = "/logout";

    @Test
    public void logoutUserTest() throws ServletException, IOException {
        LogoutServlet logoutServlet = mock(LogoutServlet.class);



        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        logoutServlet.doPost(request,response);
//        when(logoutServlet.getCurrentInstance().getExternalContext().getRequest()).thenReturn(request);
        when(request.getRequestDispatcher(path)).thenReturn(dispatcher);


        verify(response).sendRedirect(request.getContextPath());
    }
}
